function showAlert() {
	$(".button-round").addClass("disabled");
	$(".glyphicon-refresh").addClass("glyphicon-spin");
	$("#info-alert").append($('<div class="alert alert-info fade in"> \
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> \
    <strong>Info!</strong> Cinema Database start updating! \
  </div>'));
}

$(document).on("click", ".glyphicon-remove", function () {
    var id = $(this).data('id');
    $(".modal-footer .btn-danger").attr("href", function(i,str) {
    	   return str + id;
    });
 });
<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ attribute name="cinemas" required="true" type="java.util.Collection" %>
<%@ attribute name="modal_target" required="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<table class="table table-striped">
  <thead>
    <tr>
      <th>Name</th>
      <th>Address</th>
      <th width="40"></th>
      <th width="20"></th>
    </tr>
  </thead>
  <tbody>
    <c:forEach items="${cinemas}" var="cinema">
      <tr>
        <td>${cinema.name}</td>
        <td>${cinema.address}</td>
        <td><a href="<c:url value='/cinemas/edit/${cinema.id}' />">
            <span class="glyphicon glyphicon-pencil"></span>
        </a></td>
        <td><span class="glyphicon glyphicon-remove"
          data-id="${cinema.id}" data-toggle="modal"
          data-target="${modal_target}"></span></td>
      </tr>
    </c:forEach>
  </tbody>
</table>

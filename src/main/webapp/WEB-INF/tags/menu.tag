<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ attribute name="offset" required="true" %>
<nav class="navbar navbar-inverse" data-spy="affix" data-offset-top="${offset}"> 
    <div class="container-fluid"> 
      <ul class="nav navbar-nav"> 
        <li><a href="/moviebuddy"> 
        <span class="glyphicon glyphicon-home"></span> Home</a></li> 
        <li class="active"><a href="#">Cinemas</a></li> 
        <li><a href="#">Movies</a></li> 
        <li><a href="#">Sessions</a></li> 
      </ul> 
    </div> 
</nav>
<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>Cinema Registration Form</title>
  <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
  <link href="<c:url value='/resources/css/cinema-form.css'/>" rel="stylesheet"></link>

</head>
<body>
  <div class="container-fluid center" >
    <div class="panel panel-default cinema-form">
    <h1>Cinema Form</h1>
    <hr>
    <form:form method="POST" modelAttribute="cinema" class="form-horizontal">
    <form:input type="hidden" path="id" id="id"/>
        <div class="form-group">
          <label class="col-md-4 control-lable" for="name">Cinema name:</label>
          <div class="col-md-8">
            <c:choose>
              <c:when test="${edit}">
                <form:input type="text" path="name" id="name"
                  class="form-control input-sm" readonly="true" />
              </c:when>
              <c:otherwise>
                <form:input type="text" path="name" id="name"
                  class="form-control input-sm" placeholder="Enter cinema name"/>
                <div class="has-error">
                  <form:errors path="name" class="help-inline" />
                </div>
              </c:otherwise>
            </c:choose>
          </div>
        </div>

        <div class="form-group">
          <label class="col-md-4 control-lable" for="address">Address:</label>
          <div class="col-md-8">
            <form:input type="text" path="address" id="address"
              class="form-control input-sm" placeholder="Enter address"/>
            <div class="has-error">
              <form:errors path="address" class="help-inline" />
            </div>
          </div>
        </div>
      

        <div class="form-group">
          <label class="col-md-4 control-lable" for="lat">Latitude:</label>
          <div class="col-md-8">
            <form:input type="number" step="any" path="lat" id="lat"
              class="form-control input-sm" placeholder="Enter latitude"/>
            <div class="has-error">
              <form:errors path="lat" class="help-inline" />
            </div>
          </div>
        </div>
      
        <div class="form-group">
          <label class="col-md-4 control-lable" for="lon">Longitude:</label>
          <div class="col-md-8">
            <form:input type="number" step="any" path="lon" id="lon"
              class="form-control input-sm" placeholder="Enter longitude"/>
            <div class="has-error">
              <form:errors path="lon" class="help-inline" />
            </div>
          </div>
        </div>

        <div class="form-actions text-right">
          <c:choose>
            <c:when test="${edit}">
              <a class="btn btn-default" href="<c:url value='/cinemas' />">Cancel</a>
            <input type="submit" value="Update" class="btn btn-success" />
            </c:when>
            <c:otherwise>
            <a class="btn btn-default" href="<c:url value='/cinemas' />">Cancel</a>
            <input type="submit" value="Add" class="btn btn-success" /> 
            </c:otherwise>
          </c:choose>
        </div>
    </form:form>
    </div>
  </div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="custom" %>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>Cinema List</title>
  <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
  <link href="<c:url value='/resources/css/main.css'/>" rel="stylesheet"></link>
    
</head>
<body>
  <div class="container-fluid header-cinema" >
  <div class="col-md-11">
  <h1>Cinema Database</h1>
  </div>
  <div class="col-md-1 btn-success button-round"><a href="/moviebuddy/cinemas/update" onClick="showAlert();">
        <span class="glyphicon glyphicon-refresh"></span>
        </a>
  </div>
  </div>
  <custom:menu offset="90"></custom:menu>  
  <div class="container-fluid">
    <div id="info-alert"></div>
    <c:if test="${success != null}">
      <div class="alert alert-success fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> ${success}
      </div>  
    </c:if>
    <c:if test="${error != null}">
      <p class="alert alert-danger fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Error!</strong> ${error}
      </p>  
    </c:if>
    <a href="<c:url value='/cinemas/new' />" 
      class="btn btn-success btn-lg btn-block">Add new cinema
    </a>
    <div class="panel panel-default">  
      <custom:cinemalist cinemas="${cinemas}" modal_target="#confirmDelete"></custom:cinemalist>
    </div>
  </div>
  
  <!-- Modal -->
  <div class="modal fade" id="confirmDelete" role="dialog">
    <div class="modal-dialog modal-sm">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete confirmation</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure?</p>
        </div>
        <div class="modal-footer">
          <a href="<c:url value='/cinemas/delete/' />" class="btn btn-danger" role="button">Delete</a> 
        </div>
      </div>
      
    </div>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="resources/js/menu.js"></script>
</body>
</html>
package parser;

public interface JsonableParser {
	String parseAsJson(String url);
}

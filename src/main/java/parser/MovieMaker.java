package parser;

import java.time.LocalDate;
import java.util.Set;

import database.entity.Country;
import database.entity.Genre;
import database.entity.IMDb;
import database.entity.Keyword;
import database.entity.Movie;
import database.entity.Staff;

public interface MovieMaker {
	 String parseTitle();
	 String parseOriginalTitle();
	 Integer parseProductionYear();
	 String parseTagline();
	 LocalDate parseReleaseDate();
	 Integer parseRuntime();
	 String parseDescription();
	 IMDb parseImdb();
	 String parseImageRef();
	 Set<Genre> parseGenres();
	 Set<Country> parseCountries();
	 Set<Keyword> parseKeywords();
	 Set<Staff> parseStaffs();
	 
	 Movie makeMovie();
}

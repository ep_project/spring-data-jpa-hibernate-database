package parser;

public interface Parser<T> extends JsonableParser{

	T parse(final String url);
	
}

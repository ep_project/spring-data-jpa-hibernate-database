package parser.afishayandex;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import database.entity.Cinema;
import parser.PhantomParser;

public class AfishaYandexCinemaParser extends PhantomParser<List<Cinema>> {
	
	final static Logger logger = Logger.getLogger(AfishaYandexCinemaParser.class);
	
	@Override
	public List<Cinema> parse(String url) {
		List<WebElement> cinemaElements = getCinemaElements(url);
		List<Cinema> cinemas = new ArrayList<Cinema>();
		for (WebElement cinemaElement : cinemaElements) {
			Cinema cinema = new Cinema();
			cinema.setName(parse(CinemaSelectors.CINEMA_TITLE, cinemaElement));
			cinema.setAddress(parse(CinemaSelectors.CINEMA_ADRESS, cinemaElement));
			cinemas.add(cinema);
			logger.info(String.format("parsed cinema {%s}",cinema.getName()));
		}
		
		return cinemas;
	}
	
	@Override
	public String parseAsJson(String url) {
		logger.debug("start processing");
		
		List<Cinema> cinemas = parse(url);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		
		String res = gson.toJson(cinemas);
		logger.info("parsed all cinemas");
		logger.debug(res);
		logger.debug("done");
		return res;
	}
	
	private List<WebElement> getCinemaElements(String url) {
		driver.get(url);
		openAllCinemas();
		return driver.findElements(CinemaSelectors.CINEMA_ELEMENTS.getSelector());
	}

	private void openAllCinemas() {
		while (hasMoreButton()) {
			driver.findElement(CinemaSelectors.MORE_CINEMA_BUTTON.getSelector()).click();
		}
	}
	
	private boolean hasMoreButton() {
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(CinemaSelectors.MORE_CINEMA_BUTTON.getSelector()));
		List<WebElement> buttons = driver.findElements(CinemaSelectors.MORE_CINEMA_BUTTON.getSelector());
		assert buttons.size() == 1;
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			logger.error("Thread was interrupted!", e);
		}
		return buttons.get(0).isDisplayed();
	}
	
	private String parse(CinemaSelectors selector, WebElement el) {
		return el.findElement(selector.getSelector()).getAttribute("innerText");
	}

}

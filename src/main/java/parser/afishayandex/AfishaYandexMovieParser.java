package parser.afishayandex;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import database.entity.Country;
import database.entity.Genre;
import database.entity.IMDb;
import database.entity.Keyword;
import database.entity.Movie;
import database.entity.Person;
import database.entity.Profession;
import database.entity.Staff;
import parser.MovieMaker;

public class AfishaYandexMovieParser extends AfishaYandexEventParser<List<Movie>> implements MovieMaker
{	
	final static Logger logger = Logger.getLogger(AfishaYandexMovieParser.class);

	@Override
	public List<Movie> parse(String url) {
		List<WebElement> elements = getMovieElements(url);
		List<Movie> movies = new ArrayList<Movie>();
		for (WebElement el : elements) {
			waitAndClick(el.findElement(By.cssSelector("a")));
			Movie movie = makeMovie();
			movies.add(movie);
			logger.info(String.format("parsed movie {%s}",movie.getTitle()));
		}
		
		return movies;
	}
	
	@Override
	public String parseAsJson(String url) {
		logger.debug("start processing");
		List<Movie> movies = parse(url);
		logger.info("parsed all movies");
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String res = gson.toJson(movies);
		logger.debug(res);
		logger.debug("done");
		return res;
	}
	
	@Override
	public Movie makeMovie() {
//		List<WebElement> el = driver.findElements(SessionSelectors.KINOPOISK_MOVIE_ID.getSelector());
//		if (!el.isEmpty()) {
//			String href = el.get(0).getAttribute("href");
//			try(KinopoiskMovieParser p = new KinopoiskMovieParser()) {
//				return p.parse(href);
//			}
//		}
		
		Movie movie = new Movie();
		
		movie.setTitle(parseTitle());
		movie.setOriginalTitle(parseOriginalTitle());
		movie.setProductionYear(parseProductionYear());
		movie.setRuntime(parseRuntime());
		movie.setDescription(parseDescription());
		movie.setImdb(parseImdb());
		movie.setCountries(parseCountries());
		movie.setReleaseDate(parseReleaseDate());
		movie.setStaffs(parseStaffs());
		
		return movie;
	}

	@Override
	public String parseImageRef() {
		// TODO parse image
		return null;
	}
	
	@Override
	public String parseTitle() {
		return getTitle();
	}

	@Override
	public String parseOriginalTitle() {
		List<WebElement> el = driver.findElements(SessionSelectors.ORIGINAL_TITLE.getSelector());
		if (el.isEmpty()) return null;
		return el.get(0).getAttribute("innerText");
	}

	@Override
	public Integer parseProductionYear() {
		return getProductionYear();
	}

	@Override
	public String parseTagline() {
		//no tagline
		return null;
	}

	@Override
	public LocalDate parseReleaseDate() {
		List<WebElement> el = driver.findElements(SessionSelectors.RELEASE_DATE.getSelector());
		if (el.isEmpty()) return null;
		LocalDate date = LocalDate.parse(el.get(0).getAttribute("innerText"), DateTimeFormatter.ofPattern("d MMMM yyyy", new Locale("ru")));
		return date;
	}

	@Override
	public Integer parseRuntime() {
		List<WebElement> el = driver.findElements(SessionSelectors.RUNTIME.getSelector());
		if (el.isEmpty()) return null;
		return Integer.parseInt(el.get(0).getAttribute("innerText").split(" ")[0]);
	}

	@Override
	public String parseDescription() {
		List<WebElement> el = driver.findElements(SessionSelectors.DESCRIPTION.getSelector());
		if (el.isEmpty()) return null;
		return el.get(0).getAttribute("innerText");
	}

	@Override
	public IMDb parseImdb() {
		List<WebElement> el = driver.findElements(SessionSelectors.IMDb.getSelector());
		if (el.isEmpty()) return null;
		return new IMDb(Float.parseFloat(el.get(0).getAttribute("innerText")));
	}

	@Override
	public Set<Genre> parseGenres() {
		// no genres
		return null;
	}

	@Override
	public Set<Country> parseCountries() {
		List<WebElement> el = driver.findElements(SessionSelectors.COUNTRIES.getSelector());
		if (el.isEmpty()) return null;
		Set<Country> set = new HashSet<Country>();
		String [] countries = el.get(0).getAttribute("innerText").split(", ");
		for (String country : countries) {
			set.add(new Country(country));
		}
		return set;
	}

	@Override
	public Set<Keyword> parseKeywords() {
		// no keywords
		return null;
	}

	@Override
	public Set<Staff> parseStaffs() {
		List<WebElement> el = driver.findElements(SessionSelectors.DIRECTORS.getSelector());
		Set<Staff> set = new HashSet<Staff>();
		if (!el.isEmpty()) {
			String [] directors = el.get(0).getAttribute("innerText").split(", ");
			for (String director : directors) {
				set.add(new Staff(new Person(director), new Profession("director")));
			}
		}
		el = driver.findElements(SessionSelectors.ACTORS.getSelector());
		if (!el.isEmpty()) {
			String [] actors = el.get(0).getAttribute("innerText").split(", ");
			for (String actor : actors) {
				set.add(new Staff(new Person(actor), new Profession("star")));
			}
		}
		return set;
	}
	
}

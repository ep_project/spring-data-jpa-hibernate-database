package database.service;

import java.io.IOException;
import java.util.List;

import database.entity.Cinema;

public interface CinemaService {
	Cinema addCinema(Cinema cinema);
	Cinema updateCinema(Cinema cinema);
	Cinema addOrUpdateCinema(Cinema cinema);
  void delete(long id);
  Cinema getByName(String name);
  Cinema getById(long id);
  List<Cinema> getAll();
  void updateCinemaDB()throws IOException;
}

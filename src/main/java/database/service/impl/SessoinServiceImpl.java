package database.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import database.entity.Format;
import database.entity.Session;
import database.repository.CinemaRepository;
import database.repository.FormatRepository;
import database.repository.MovieRepository;
import database.repository.SessionRepository;
import database.service.SessionService;

@Service
public class SessoinServiceImpl implements SessionService{

	@Autowired
	private SessionRepository sessionRepository;
	@Autowired
	private FormatRepository formatRepository;
	@Autowired
	private CinemaRepository cinemaRepository;
	@Autowired
	private MovieRepository movieRepository;
	
	@Override
	public Session addSession(Session session) {
		
		Format format = session.getFormat();
		if (format != null) {
			Format foundedFormat = formatRepository.findByName(format.getName());
			if (foundedFormat == null) {
				format = formatRepository.saveAndFlush(format);
			} else {
				format.setId(foundedFormat.getId());
			}
		}
		
		session.setCinema(cinemaRepository.findByName(session.getCinema().getName()));
		session.setMovie(movieRepository.findByTitleAndProductionYear(
				session.getMovie().getTitle(), session.getMovie().getProductionYear()));
		return sessionRepository.saveAndFlush(session);
	}

	@Override
	public void delete(Long id) {
		sessionRepository.delete(id);
	}

	@Override
	public void dropAll() {
		sessionRepository.deleteAllInBatch();
	}

	@Override
	public List<Session> getAll() {
		return sessionRepository.findAll();
	}

}

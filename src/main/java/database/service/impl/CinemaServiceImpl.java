package database.service.impl;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import database.entity.Cinema;
import database.repository.CinemaRepository;
import database.service.CinemaService;
import parser.Urls;
import parser.afishayandex.AfishaYandexCinemaParser;

@Service
public class CinemaServiceImpl implements CinemaService {
	final static private String CINEMA_JSON_PATH = "/home/shest/cinemas/cinemas.json";

	final static Logger logger = Logger.getLogger(CinemaServiceImpl.class);
	
	@Autowired
	private CinemaRepository cinemaRepository;
	
	@Override
	public Cinema addOrUpdateCinema(Cinema cinema) {
		Cinema old = getByName(cinema.getName());
		if (old != null) {
			cinema.setId(old.getId());
		}
		return cinemaRepository.saveAndFlush(cinema);
	}

	@Override
	public void delete(long id) {
		cinemaRepository.delete(id);
		
	}

	@Override
	public Cinema getByName(String name) {
		return cinemaRepository.findByName(name);
	}

	@Override
	public List<Cinema> getAll() {
		return cinemaRepository.findAll(new Sort(Sort.Direction.ASC, "name"));
	}
	
	@Override
	public void updateCinemaDB() throws IOException {
		
    logger.info("start updating cinema database");
		
		parseCinemas();
		
		Gson gson = new Gson();
		JsonReader reader = null;
		
		reader = new JsonReader(new FileReader(new File(CINEMA_JSON_PATH)));
		
		Cinema[] cinemas = gson.fromJson(reader, Cinema[].class);
		for (Cinema c : cinemas) {
			addOrUpdateCinema(c);
		}
		
    logger.info("finish updating cinema database");
	}

	private void parseCinemas() throws IOException {
		try (AfishaYandexCinemaParser parser = new AfishaYandexCinemaParser();
			FileWriter writer = new FileWriter(new File(CINEMA_JSON_PATH))) {
			String json = parser.parseAsJson(Urls.CINEMAS_SPB.getUrl());
			writer.write(json);
		} 
	}

	@Override
	public Cinema addCinema(Cinema cinema) {
		return cinemaRepository.saveAndFlush(cinema);
	}

	@Override
	public Cinema getById(long id) {
		return cinemaRepository.findOne(id);
	}

	@Override
	public Cinema updateCinema(Cinema cinema) {
		return cinemaRepository.saveAndFlush(cinema);
	}

}

package database.service.impl;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import database.entity.IMDb;
import database.entity.Movie;
import database.entity.Person;
import database.entity.Profession;
import database.entity.SimpleEntity;
import database.entity.Staff;
import database.entity.Staff.Pk;
import database.repository.BaseRepository;
import database.repository.CountryRepository;
import database.repository.GenreRepository;
import database.repository.KeywordRepository;
import database.repository.MovieRepository;
import database.repository.PersonRepository;
import database.repository.ProfessionRepository;
import database.repository.RoleRepository;
import database.service.MovieService;

@Service
public class MovieServiceImpl implements MovieService {

	@Autowired
	private MovieRepository movieRepository;
	@Autowired
	private CountryRepository countryRepository;
	@Autowired
	private GenreRepository genreRepository;
	@Autowired
	private KeywordRepository keywordRepository;
	@Autowired
	private PersonRepository personRepository;
	@Autowired
	private ProfessionRepository profRepository;
	@Autowired
	private RoleRepository roleRepository;
	
	@Override
	public Movie addOrUpdateMovie(Movie movie) {		
		
		Movie founded = getByTitleAndProductionYear(movie.getTitle(), movie.getProductionYear());
		
		if (founded != null) {
			movie.setId(founded.getId());		
		}
		
		IMDb imdb = movie.getImdb();
		if (imdb != null) {
			imdb.setMovie(movie);
			imdb.setId(movie.getId());
		}
		
		updateChilds(movie.getCountries(), countryRepository);
		updateChilds(movie.getGenres(), genreRepository);
		updateChilds(movie.getKeywords(), keywordRepository);
		updateChilds(movie.getPersons(), personRepository);
		updateChilds(movie.getProfessions(), profRepository);
		
		for (Staff s : movie.getStaffs()) {
			updateChilds(s.getRoles(), roleRepository);
			Person person = s.getPerson();
			Profession prof = s.getProfession();
			Pk pk = new Pk(movie.getId(), person.getId(), prof.getId());
			s.setPk(pk);
			s.setMovie(movie);
			s.setPerson(person);
			s.setProfession(prof);
		}
		
		return movieRepository.saveAndFlush(movie);
	}

	@Override
	public void delete(long id) {
		movieRepository.delete(id);		
	}

	@Override
	public Movie getByTitleAndProductionYear(String title, Integer year) {
		return movieRepository.findByTitleAndProductionYear(title, year);

	}

	@Override
	public List<Movie> getAll() {
		return movieRepository.findAll();

	}
	
	private <T extends SimpleEntity> void  updateChilds(Set<T> set, BaseRepository<T> rep) {
		for (T t : set) {
			SimpleEntity founded = rep.findByName(t.getName());
			if (founded != null) {
				t.setId(founded.getId());
			} else {
				t = rep.saveAndFlush(t);
			}
		}
	}

}

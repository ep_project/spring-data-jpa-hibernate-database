package database.service;

import java.util.List;

import database.entity.Movie;

public interface MovieService {
	Movie addOrUpdateMovie(Movie movie);
    void delete(long id);
    Movie getByTitleAndProductionYear(String title, Integer year);
    List<Movie> getAll();
}

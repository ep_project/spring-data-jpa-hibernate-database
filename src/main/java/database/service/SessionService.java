package database.service;

import java.util.List;

import database.entity.Session;

public interface SessionService {
	Session addSession(Session session);
    void delete(Long id);
    void dropAll();
    List<Session> getAll();
}

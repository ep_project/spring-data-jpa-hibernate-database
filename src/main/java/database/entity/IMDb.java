package database.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "Movie.IMDb")
public class IMDb implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	private Long id;
	
	@Column(name = "ball")
	private Float ball;
	
	@Column(name = "count")
	private Integer count;
	
	@MapsId
	@OneToOne
	@JoinColumn(name = "id", referencedColumnName = "idMovie")
	private Movie movie;
	
	public IMDb() {
	}
	
	public IMDb(Float ball) {
		this.ball = ball;
	}
	
	public IMDb(Float ball, Integer count, Movie movie) {
		this.ball = ball;
		this.count = count;
	}
	
	public Float getBall() {
		return ball;
	}
	public void setBall(Float ball) {
		this.ball = ball;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		int result;
        result = (ball != null ? ball.hashCode() : 0);
        result = 31 * result + (count != null ? count.hashCode() : 0);
        return result;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IMDb that = (IMDb) o;

        if (ball != null ? !ball.equals(that.ball) : that.ball != null) return false;
        if (count != null ? !count.equals(that.count) : that.count != null) return false;

        return true;
	}
	
}

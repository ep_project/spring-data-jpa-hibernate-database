package database.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "Movie.Movies")
public class Movie implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name= "increment", strategy= "increment")
    @Column(name = "idMovie", length = 8, nullable = false, unique = true)
    private long id;

	@Column(name = "title", nullable = false)
	private String title;
	
	@Column(name = "original_title")
	private String originalTitle;
	
//	private String imageRef;
	
	@Column(name = "production_year")
	private Integer productionYear;
	
	@Column(name = "tagline")
	private String tagline;
	
	@Column(name = "release_date")
	private LocalDate releaseDate;
	
	@Column(name = "runtime")
	private Integer runtime;
	
	@Column(name = "description", length = 250)
	private String description;
	
	@OneToOne(orphanRemoval = true)
	@PrimaryKeyJoinColumn
	@Cascade({CascadeType.ALL})
	private IMDb imdb;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@Cascade({CascadeType.SAVE_UPDATE})
    @JoinTable(name = "Movie.Genres_Movies", 
    	joinColumns = {@JoinColumn(name = "idMovie")}, 
    	inverseJoinColumns = {@JoinColumn(name = "idGenre")})
	private Set<Genre> genres = new HashSet<>();
	
	@ManyToMany(fetch = FetchType.LAZY)
	@Cascade({CascadeType.SAVE_UPDATE})
    @JoinTable(name = "Movie.Countries_Movies", 
    	joinColumns = {@JoinColumn(name = "idMovie")}, 
    	inverseJoinColumns = {@JoinColumn(name = "idCountry")})
	private Set<Country> countries = new HashSet<>();
	
	@ManyToMany(fetch = FetchType.LAZY)
	@Cascade({CascadeType.SAVE_UPDATE})
    @JoinTable(name = "Movie.Movie_Keywords", 
    	joinColumns = {@JoinColumn(name = "idMovie")}, 
    	inverseJoinColumns = {@JoinColumn(name = "idKeyword")})
	private Set<Keyword> keywords = new HashSet<>();
	
	@OneToMany(mappedBy = "movie", fetch = FetchType.LAZY, orphanRemoval = true)
	@Cascade({CascadeType.ALL})
	private Set<Staff> staffs = new HashSet<>();
		
	public Movie() {
	}
	
	public Movie(String title, int productionYear) {
		this.title = title;
		this.productionYear = productionYear;
	}
	
	public Movie(String title, String originalTitle, int productionYear, String tagline, LocalDate releaseDate, int runtime,
			String description, IMDb imdb, Set<Genre> genres, Set<Country> countries, Set<Keyword> keywords,
			Set<Staff> staffs) {
		this.title = title;
		this.originalTitle = originalTitle;
		this.productionYear = productionYear;
		this.tagline = tagline;
		this.releaseDate = releaseDate;
		this.runtime = runtime;
		this.description = description;
		this.imdb = imdb;
		this.genres = genres;
		this.countries = countries;
		this.keywords = keywords;
		this.staffs = staffs;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getOriginalTitle() {
		return originalTitle;
	}
	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}
	public Integer getProductionYear() {
		return productionYear;
	}
	public void setProductionYear(Integer productionYear) {
		this.productionYear = productionYear;
	}
	public String getTagline() {
		return tagline;
	}
	public void setTagline(String tagline) {
		this.tagline = tagline;
	}
	public LocalDate getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(LocalDate releaseDate) {
		this.releaseDate = releaseDate;
	}
	public Integer getRuntime() {
		return runtime;
	}
	public void setRuntime(Integer runtime) {
		this.runtime = runtime;
	}
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		try {
	        int size = getClass().getDeclaredField("description").getAnnotation(Column.class).length();
	        int inLength = (description == null ? 0 : description.length());
	        if (inLength > size) {
	        	description = description.substring(0, description.substring(0, size - 3).lastIndexOf(" ")) + "...";
	        }
		} catch (NoSuchFieldException ex) {
		} catch (SecurityException ex) {
		}
		this.description = description;
	}
	
	
	public IMDb getImdb() {
		return imdb;
	}
	
	public void setImdb(IMDb imdb) {
		this.imdb = imdb;
	}
	
	public Set<Genre> getGenres() {
		return genres;
	}
	public void setGenres(Set<Genre> genres) {
		this.genres = genres;
	}
	public Set<Country> getCountries() {
		return countries;
	}
	public void setCountries(Set<Country> countries) {
		this.countries = countries;
	}
	public Set<Keyword> getKeywords() {
		return keywords;
	}
	public void setKeywords(Set<Keyword> keywords) {
		this.keywords = keywords;
	}
	public Set<Staff> getStaffs() {
		return staffs;
	}
	public void setStaffs(Set<Staff> staffs) {
		this.staffs = staffs;
	}
	
	public Set<Person> getPersons() {
		Set<Person> set = new HashSet<>();
		for (Staff staff : staffs) {
			set.add(staff.getPerson());
		}
		return set;
	}
	
	public Set<Profession> getProfessions() {
		Set<Profession> set = new HashSet<>();
		for (Staff staff : staffs) {
			set.add(staff.getProfession());
		}
		return set;
	}
	
//	public String getImageRef() {
//		return imageRef;
//	}
//
//	public void setImageRef(String imageRef) {
//		this.imageRef = imageRef;
//	}

	@Override
	public int hashCode() {
		int result;
        result = (title != null ? title.hashCode() : 0);
        result = 31 * result + (productionYear != null ? productionYear.hashCode() : 0);
        return result;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Movie that = (Movie) o;

        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (productionYear != null ? !productionYear.equals(that.productionYear) : that.productionYear != null) return false;

        return true;
	}
	
}

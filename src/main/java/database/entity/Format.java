package database.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "Session.Formats")
public class Format {
	
	@Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name= "increment", strategy= "increment")
    @Column(name = "idFormat", length = 8, nullable = false)
    private Long id;
	
	@Column(name = "name", nullable = false, unique = true)
	private String name;
	
	public Format() {
	}
	
	public Format(String name) {
		this.name = name;
	}
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Format that = (Format) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
       
        return true;
    }

    public int hashCode() {
        return 31 * (name != null ? name.hashCode() : 0);
    }
}

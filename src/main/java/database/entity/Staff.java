package database.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name = "Movie.Staff")
public class Staff {
	
	@Embeddable
	public static class Pk implements Serializable {
		
		private static final long serialVersionUID = 1L;

		@Column(nullable = false, updatable = false)
        private Long idMovie;

        @Column(nullable = false, updatable = false)
        private Long idPerson;

        @Column(nullable = false, updatable = false)
        private Long idProfession;

        public Pk() {}

        public Pk(Long idMovie, Long idPerson, Long idProfession) { 
        	this.idMovie = idMovie;
        	this.idPerson = idPerson;
        	this.idProfession = idProfession;
        }

		public Long getIdMovie() {
			return idMovie;
		}

		public void setIdMovie(Long idMovie) {
			this.idMovie = idMovie;
		}

		public Long getIdPerson() {
			return idPerson;
		}

		public void setIdPerson(Long idPerson) {
			this.idPerson = idPerson;
		}

		public Long getIdProfession() {
			return idProfession;
		}

		public void setIdProfession(Long idProfession) {
			this.idProfession = idProfession;
		}
		@Override
		public int hashCode() {
			int result;
	        result = (idMovie != null ? idMovie.hashCode() : 0);
	        result = 31 * result + (idPerson != null ? idPerson.hashCode() : 0);
	        result = 31 * result + (idProfession != null ? idProfession.hashCode() : 0);
	        return result;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
	        if (o == null || getClass() != o.getClass()) return false;

	        Pk that = (Pk) o;

	        if (idMovie != null ? !idMovie.equals(that.idMovie) : that.idMovie != null) return false;
	        if (idPerson != null ? !idPerson.equals(that.idPerson) : that.idPerson != null) return false;
	        if (idProfession != null ? !idProfession.equals(that.idProfession) : that.idProfession != null) return false;

	        return true;
		}
        
	}
	
	@EmbeddedId
	private Pk pk;
	
	@ManyToOne
	@JoinColumn(name = "idPerson", insertable = false, updatable = false)
	private Person person;
	
	@ManyToOne
	@JoinColumn(name = "idProfession", insertable = false, updatable = false)
	private Profession profession;
	
	@ManyToOne
	@MapsId("idMovie")
	@JoinColumn(name = "idMovie", insertable = false, updatable = false)
	private Movie movie;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@Cascade({CascadeType.SAVE_UPDATE})
    @JoinTable(name = "Movie.Actor_Roles", 
    	joinColumns = {@JoinColumn(name = "idMovie"), @JoinColumn(name = "idPerson"), @JoinColumn(name = "idProfession")}, 
    	inverseJoinColumns = {@JoinColumn(name = "idRoles")})
	private Set<Role> roles = new HashSet<>();
	
	public Staff() {
	}
	
	public Staff(Person person, Profession profession) {
		this.person = person;
		this.profession = profession;
	}
	
	public Staff(Person person, Profession profession, Set<Role> roles) {
		this.person = person;
		this.profession = profession;
//		this.roles = roles;
	}
	
	public Pk getPk() {
		return pk;
	}

	public void setPk(Pk pk) {
		this.pk = pk;
	}

	public Set<Role> getRoles() {
		return roles;
	}
	
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	public Profession getProfession() {
		return profession;
	}
	public void setProfession(Profession profession) {
		this.profession = profession;
	}
	public Movie getMovie() {
		return movie;
	}
	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Staff that = (Staff) o;

        if (movie != null ? !movie.equals(that.movie) : that.movie != null) return false;
        if (person != null ? !person.equals(that.person) : that.person != null) return false;
        if (profession != null ? !profession.equals(that.profession) : that.profession != null) return false;
        
        return true;
    }
	
	@Override
	public int hashCode() {
		int result;
        result = (movie != null ? movie.hashCode() : 0);
        result = 31 * result + (person != null ? person.hashCode() : 0);
        result = 31 * result + (profession != null ? profession.hashCode() : 0);
        return result;
	}
}

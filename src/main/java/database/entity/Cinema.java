package database.entity;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;


@Entity
@Table(name = "Cinema.Cinemas")
public class Cinema {
	
	@Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name= "increment", strategy= "increment")
    @Column(name = "idCinema", length = 8, nullable = false)
    private long id;
	
	@Column(name = "name", nullable = false, unique = true)
	@NotEmpty
	private String name;

	@Column(name = "address")
	@NotEmpty
	private String address;
	
	@Column(name = "lon")
	private Float lon;
	
	@Column(name = "lat")
	private Float lat;
	
	public Cinema() {
	}
	public Cinema(String name) {
		this.name = name;
	}
	
	public Cinema(String name, String address, float lon, float lat) {
		this.name = name;
		this.address = address;
		this.lon = lon;
		this.lat = lat;
	}
	
	public long getId() {
        return id;
    }
 
    public void setId(long id) {
        this.id = id;
    }
    
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Float getLon() {
		return lon;
	}
	public void setLon(Float lon) {
		this.lon = lon;
	}
	public Float getLat() {
		return lat;
	}
	public void setLat(Float lat) {
		this.lat = lat;
	}
	
	public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cinema that = (Cinema) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    public int hashCode() {
        return (name != null ? name.hashCode() : 0);
    }
}

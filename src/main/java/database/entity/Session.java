package database.entity;

import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "Session.Sessions")
public class Session {
	
	@Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name= "increment", strategy= "increment")
    @Column(name = "idSession", length = 8, nullable = false)
    private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@Cascade({CascadeType.SAVE_UPDATE})
    @JoinColumn(name = "idCinema", nullable = false)
	private Cinema cinema;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@Cascade({CascadeType.SAVE_UPDATE})
    @JoinColumn(name = "idMovie", nullable = false)
	private Movie movie;
	
	@Column(nullable = false)
	private Integer price;
	
	@Column(nullable = false)
	private LocalTime time;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@Cascade({CascadeType.SAVE_UPDATE})
    @JoinColumn(name = "idFormat", nullable = false)
	private Format format;
	
	public Session() {
	}
	
	public Session(Cinema cinema, Movie movie, int price, LocalTime time, Format format) {
		this.cinema = cinema;
		this.movie = movie;
		this.price = price;
		this.time = time;
		this.format = format;
	}
	
	public Cinema getCinema() {
		return cinema;
	}
	public void setCinema(Cinema cinema) {
		this.cinema = cinema;
	}
	public Movie getMovie() {
		return movie;
	}
	public void setMovie(Movie movie) {
		this.movie = movie;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public LocalTime getTime() {
		return time;
	}
	public void setTime(LocalTime time) {
		this.time = time;
	}
	public Format getFormat() {
		return format;
	}
	public void setFormat(Format format) {
		this.format = format;
	}
	
	public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Session that = (Session) o;

        if (price != null ? !price.equals(that.price) : that.price != null) return false;
        if (time != null ? !time.equals(that.time) : that.time != null) return false;
        if (format != null ? !format.equals(that.format) : that.format != null) return false;
        if (movie != null ? !movie.equals(that.movie) : that.movie != null) return false;
        if (cinema != null ? !cinema.equals(that.cinema) : that.cinema != null) return false;

        return true;
    }

    public int hashCode() {
    	int result;
        result = (movie != null ? movie.hashCode() : 0);
        result = 31 * result + (cinema != null ? cinema.hashCode() : 0);
        result = 31 * result + (time != null ? time.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (format != null ? format.hashCode() : 0);

        return result;
    }
}

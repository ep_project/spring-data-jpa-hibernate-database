package database.entity;

public interface SimpleEntity {
	
	void setName(String name);
	String getName();
	void setId(Long id);
	Long getId();
}

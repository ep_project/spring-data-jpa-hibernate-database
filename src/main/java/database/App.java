package database;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import database.config.DataConfig;
import database.entity.Cinema;
import database.entity.Movie;
import database.entity.Session;
import database.service.CinemaService;
import database.service.MovieService;
import database.service.SessionService;
import parser.Urls;
import parser.afishayandex.AfishaYandexCinemaParser;
import parser.afishayandex.AfishaYandexMovieParser;
import parser.afishayandex.AfishaYandexTicketParser;


public class App {
	
	final static Logger logger = Logger.getLogger(App.class);

	final static private String CINEMA_JSON_PATH = "/home/shest/cinemas/cinemas.json";
	final static private String MOVIE_JSON_PATH = "/home/shest/movies/movies.json";
	final static private String TICKET_JSON_PATH = "/home/shest/tickets/tickets.json";


	public static void main(String[] args) throws FileNotFoundException {
		try(AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(DataConfig.class))
		{
			new App().updateTickets(ctx);
		}
	}
	
	private void updateCinemas(ApplicationContext ctx) throws FileNotFoundException {
		CinemaService service = (CinemaService) ctx.getBean(CinemaService.class);
		Gson gson = new Gson();
		JsonReader reader = new JsonReader(new FileReader(new File(CINEMA_JSON_PATH)));
		Cinema[] cinemas = gson.fromJson(reader, Cinema[].class);
		for (Cinema c : cinemas) {
			service.addOrUpdateCinema(c);
		}
	}
	
	private void updateMovies(ApplicationContext ctx) throws FileNotFoundException {
		MovieService service = (MovieService) ctx.getBean(MovieService.class);
		Gson gson = new Gson();
		JsonReader reader = new JsonReader(new FileReader(new File(MOVIE_JSON_PATH)));
		Movie[] movies = gson.fromJson(reader, Movie[].class);
		for (Movie m : movies) {
			service.addOrUpdateMovie(m);
		}
	}
	
	private void updateTickets(ApplicationContext ctx) throws FileNotFoundException {
		SessionService service = (SessionService) ctx.getBean(SessionService.class);
		JsonReader reader = new JsonReader(new FileReader(new File(TICKET_JSON_PATH)));
		Gson gson = new Gson();
		Session[] sessions = gson.fromJson(reader, Session[].class);
		service.dropAll();
		for (Session s : sessions) {
			service.addSession(s);
		}
	}
	
	private void parseCinemas() {
		try(AfishaYandexCinemaParser parser = new AfishaYandexCinemaParser();
				FileWriter	writer = new FileWriter(new File(CINEMA_JSON_PATH))) 
		{
				String json = parser.parseAsJson(Urls.CINEMAS_SPB.getUrl());
				writer.write(json);
		} catch (IOException e) {
			logger.error("Can not write to file!", e);
		} 
	}
	
	private void parseMovies() {
		try(AfishaYandexMovieParser parser = new AfishaYandexMovieParser();
				FileWriter	writer = new FileWriter(new File(MOVIE_JSON_PATH))) 
		{
				String json = parser.parseAsJson(Urls.TICKETS_SPB_TODAY.getUrl());
				writer.write(json);
		} catch (IOException e) {
			logger.error("Can not write to file!", e);
		} 
	}
	
	private void parseTickets() {
		try(AfishaYandexTicketParser parser = new AfishaYandexTicketParser();
				FileWriter	writer = new FileWriter(new File(TICKET_JSON_PATH))) 
		{
				String json = parser.parseAsJson(Urls.TICKETS_SPB_TODAY.getUrl());
				writer.write(json);
		} catch (IOException e) {
			logger.error("Can not write to file!", e);
		} 
	}
}

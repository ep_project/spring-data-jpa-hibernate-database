package database.repository;

import org.springframework.data.repository.query.Param;

public interface BaseRepository<T> {
	
	
	T findByName(@Param("name") String name);
	
	T saveAndFlush(T t);
}

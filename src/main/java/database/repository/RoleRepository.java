package database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import database.entity.Role;

public interface RoleRepository extends BaseRepository<Role>, JpaRepository<Role, Long>{
	
	@Override
	@Query("select r from Role r where r.name = :name")
    Role findByName(@Param("name") String name);
}

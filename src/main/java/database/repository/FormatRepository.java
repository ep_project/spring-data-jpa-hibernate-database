package database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import database.entity.Format;

public interface FormatRepository extends JpaRepository<Format, Long> {
	@Query("select f from Format f where f.name = :name")
    Format findByName(@Param("name") String name);
}

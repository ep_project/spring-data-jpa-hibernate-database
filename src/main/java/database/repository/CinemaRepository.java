package database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import database.entity.Cinema;

public interface CinemaRepository extends JpaRepository<Cinema, Long>{
	
	@Query("select c from Cinema c where c.name = :name")
    Cinema findByName(@Param("name") String name);
	
}

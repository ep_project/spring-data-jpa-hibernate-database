package database.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import database.entity.Movie;

public interface MovieRepository extends JpaRepository<Movie, Long> {
	
	@Query("select m from Movie m where m.title = :title AND m.productionYear = :year")
    Movie findByTitleAndProductionYear(@Param("title") String title, 
    								   @Param("year") Integer year);
	
}

package database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import database.entity.Genre;

public interface GenreRepository extends BaseRepository<Genre>,JpaRepository<Genre, Long>{
	
	@Override
	@Query("select g from Genre g where g.name = :name")
    Genre findByName(@Param("name") String name);

}

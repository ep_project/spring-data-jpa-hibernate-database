package database.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import database.entity.Session;

public interface SessionRepository extends JpaRepository<Session, Long> {
	
}

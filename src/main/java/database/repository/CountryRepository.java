package database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import database.entity.Country;

public interface CountryRepository extends BaseRepository<Country>, JpaRepository<Country, Long>{
	
	@Override
	@Query("select c from Country c where c.name = :name")
    Country findByName(@Param("name") String name);
	
}

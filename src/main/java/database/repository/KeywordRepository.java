package database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import database.entity.Keyword;

public interface KeywordRepository extends BaseRepository<Keyword>,JpaRepository<Keyword, Long>{
	@Override
	@Query("select k from Keyword k where k.name = :name")
    Keyword findByName(@Param("name") String name);
}

package database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import database.entity.Person;

public interface PersonRepository extends BaseRepository<Person>, JpaRepository<Person, Long>{
	
	@Query("select p from Person p where p.name = :name")
    Person findByName(@Param("name") String name);
}

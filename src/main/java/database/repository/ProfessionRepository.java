package database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import database.entity.Profession;

public interface ProfessionRepository extends BaseRepository<Profession>, JpaRepository<Profession, Long>{
	@Query("select p from Profession p where p.name = :name")
    Profession findByName(@Param("name") String name);
}

package database.controller;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import database.entity.Cinema;
import database.service.CinemaService;

@Controller
public class CinemaController {
	private final Logger logger = Logger.getLogger(CinemaController.class);

	private final CinemaService cinemaService;

	@Autowired
	public CinemaController(CinemaService cinemaService) {
		this.cinemaService = cinemaService;
	}
	
	@Autowired
  MessageSource messageSource;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(ModelMap model) {

		logger.debug("index() is executed!");

		return "index";
	}

	// retrieve all cinemas
	@RequestMapping(value = "/cinemas", method = RequestMethod.GET)
	public String listAllCinemas(ModelMap model) {

		logger.debug("listAllCinemas() is executed");

		List<Cinema> cinemas = cinemaService.getAll();
		model.addAttribute("cinemas", cinemas);
		
		
		
		return "cinemalist";
	}
	
	@RequestMapping(value = "/cinemas/new", method = RequestMethod.GET)
  public String newCinema(ModelMap model) {
      Cinema cinema = new Cinema();
      model.addAttribute("cinema", cinema);
      model.addAttribute("edit", false);
      return "cinema-edit";
  }
	
	@RequestMapping(value = "/cinemas/new", method = RequestMethod.POST)
  public String saveCinema(@Valid Cinema cinema, BindingResult result,
          ModelMap model, RedirectAttributes redirectAttributes) {
		
		if (result.hasErrors()) {
      return "cinema-edit";
    }

    if(cinemaService.getByName(cinema.getName()) != null){
    	FieldError nameError = new FieldError("cinema", "name", messageSource.getMessage("non.unique.name", 
    			new String[]{cinema.getName()}, Locale.getDefault()));
      result.addError(nameError);
    	return "cinema-edit";
    }
    
		cinemaService.addCinema(cinema);
		
    redirectAttributes.addFlashAttribute("success", "Cinema " + cinema.getName() + " added successfully");
    return "redirect:/cinemas";
  }
	
	@RequestMapping(value = "/cinemas/edit/{id}", method = RequestMethod.GET)
  public String editCinema(@PathVariable Long id, ModelMap model) {
      Cinema cinema = cinemaService.getById(id);
      model.addAttribute("cinema", cinema);
      model.addAttribute("edit", true);
      return "cinema-edit";
  }
   
  @RequestMapping(value = "/cinemas/edit/{id}", method = RequestMethod.POST)
  public String updateCinema(@Valid @ModelAttribute("cinema") Cinema cinema, BindingResult result,
          ModelMap model, RedirectAttributes redirectAttributes) {

    logger.info("updateCinema() is executed");

      if (result.hasErrors()) {
          return "cinema-edit";
      }
      
  		cinemaService.updateCinema(cinema);

      redirectAttributes.addFlashAttribute("success", "Cinema " + cinema.getName() + " updated successfully");
      return "redirect:/cinemas";
  }
  
  @RequestMapping(value = { "/cinemas/delete/{id}" }, method = RequestMethod.GET)
  public Callable<String> deleteCinema(@PathVariable Long id, 
  		RedirectAttributes redirectAttributes, ModelMap model) {
		logger.debug("deleteCinema() is executed");
		
		Callable<String> asyncTask = new Callable<String>() {
			 
      @Override  
      public String call() throws DataAccessException {
      	cinemaService.delete(id);
      	model.addAttribute("cinemas", cinemaService.getAll());
        redirectAttributes.addFlashAttribute("success", "Cinema was deleted!");
      	return "redirect:/cinemas";
      }
    };
		
		return asyncTask;
    
  }

	@RequestMapping(value = "/cinemas/update", method = RequestMethod.GET)
	public Callable<String> updateCinemas(ModelMap model, RedirectAttributes redirectAttributes) {

		logger.debug("updateCinemas() is executed");
		
		Callable<String> asyncTask = new Callable<String>() {
			 
      @Override
      public String call() throws IOException {
      	cinemaService.updateCinemaDB();
        redirectAttributes.addFlashAttribute("success", "Cinema Database has been updated!");
        return "redirect:/cinemas"; 
      }
    };
		
		return asyncTask;
	}
	
	@RequestMapping(value = "/cinemas/update-error", method = RequestMethod.GET)
  public String updateError(ModelMap model, RedirectAttributes redirectAttributes) {
      redirectAttributes.addFlashAttribute("error", "Cinema Database has not been updated!");
      return "redirect:/cinemas";
  }
	
	@RequestMapping(value = "/cinemas/database-error", method = RequestMethod.GET)
  public String DatabaseError(ModelMap model, RedirectAttributes redirectAttributes) {
      redirectAttributes.addFlashAttribute("error", "Data Access exception!");
      return "redirect:/cinemas";
  }

	
	@ExceptionHandler(IOException.class)
	public String handleIOException(HttpServletRequest request, Exception ex){
		logger.error("Requested URL = " + request.getRequestURL());
		logger.error("Exception Raised = " + ex);
		
		return "forward:/cinemas/update-error";
	}
	
	@ExceptionHandler(DataAccessException.class)
	public String handleDataAccessException(HttpServletRequest request, Exception ex){
		logger.error("Requested URL = " + request.getRequestURL());
		logger.error("Exception Raised = " + ex);
		return "forward:/cinemas/database-error";
	}

}
